---
layout: default
title: Spring topics
permalink: topics/spring.html
---

# Spring topics

Spring .- The Spring Framework provides a comprehensive programming and configuration model, is a lightweight, loosely coupled and integrated framework for developing enterprise applications.

Modules of spring framework:
 *  Test
 *  Spring Core Container
 *  AOP, Aspects and Instrumentation
 *  Data Access/Integration (ORM)
 *  Web
 *  Security
 *  Spring MVC

IoC .- The IoC container is responsible to instantiate, configure and assemble the objects. The main tasks performed by IoC container are:
 *  To instantiate the application class
 *  To configure the object
 *  To assemble the dependencies between the objects There are two types of IoC containers. They are:


 *  BeanFactory .-The XmlBeanFactory  is the implementation class for the BeanFactory interface. (applicationContext.xml).
 *  ApplicationContext .-The ClassPathXmlApplicationContext class is the implementation class of ApplicationContext interface.  

DI .-Dependency Injection. It's a form of IoC, where implementations are passed into an object through constructors/setters/service lockups, and make our application loosely coupled.

Different types of IoC (dependency injection)
 *  Constructor-based dependency injection− Constructor-based DI is accomplished when the container invokes a class constructor with a number of arguments, each representing a dependency on other class.
 *  Setter-based dependency injection− Setter-based DI is accomplished by the container calling setter methods on your beans after invoking a no-argument constructor or no-argument static factory method to instantiate your bean.

AOP .- Is a way for adding behavior to existing code without modifying that code.

MVC .-Is a design pattern which provides a solution to layer an application by separating Business (Model), Presentation (View) and Control Flow (Controller).


 *  The Model .-Contains the business logic.
 *  The View .-Component contains the presentation part of the application.
 *  The Controller .-Is responsible for the redirection and the interaction between View component and Model.

MVC diagram.

web.xml .- Is a configuration file to describe how a web application should be deployed.

DispatcherServlet .-The Spring Web MVC framework is designed around a DispatcherServlet that handles all the HTTP requests and responses. it loads the spring bean configuration file and initialize all the beans that are configured
 *  It's the front controller class that initializes the context based on the spring beans configurations. <servlet>    <servlet-name>dispatcher</servlet-name>    <servlet-class>        org.springframework.web.servlet.DispatcherServlet    </servlet-class>    <init-param>        <param-name>contextConfigLocation</param-name>        <param-value>/WEB-INF/spring/dispatcher-config.xml</param-value>    </init-param>    <load-on-startup>1</load-on-startup> </servlet>


 *  If annotations are enabled, it also scans the packages and configure any bean annotated with @Component, @Controller, @Repository or @Service annotations.

ContextListener .-It is the listener to start up and shut down Spring’s root WebApplicationContext when we have multiple ApplicationContext we can use it to define shared beans that can be used across different spring contexts.

ContextLoaderListener .-Is the listener class used to load root context and define spring bean configurations that will be visible to all other contexts. It’s configured in web.xml. Belongs to the package org.springframework.web.context.ContextLoaderListener starts and stops WebApplicationContext. ContextLoaderListener is registered in web.xml

<listener> <listener-class> org.springframework.web.context.ContextLoaderListener </listener-class> </listener>        <context-param>                <param-name>contextConfigLocation</param-name>                <param-value>/WEB-INF/dispatcher-servlet.xml</param-value>        </context-param>

if you want to load other Spring context XML files as well while loading the app and you can specify.   

ApplicationContext .-Is the central interface within a Spring application for providing configuration information to the application and provides:
 *  Bean factory methods for accessing application components.
 *  The ability to load file resources in a generic fashion.
 *  The ability to publish events to registered listeners.
 *  The ability to resolve messages to support internationalization.
 *  Inheritance from a parent context.

Bean (In Spring Framework) .-Any normal java class that is initialized by Spring IoC container Theses objects form the backbone of your application and that are managed by the Spring IoC container.

Bean Scope (In Spring Framework) .-It a behavior that you can specify to any bean when you defined it. Spring Framework supports the following five scopes, three of which are available only if you use a web-aware ApplicationContext.


 *  Singleton. This scopes the bean definition to a single instance per Spring IoC container (default).
 *  Prototype. This scopes a single bean definition to have any number of object instances.
 *  Request. This scopes a bean definition to an HTTP request. Only valid in the context of a webaware Spring ApplicationContext.
 *  Session. This scopes a bean definition to an HTTP session. Only valid in the context of a webaware Spring ApplicationContext.
 *  Global-session. This scopes a bean definition to a global HTTP session. Only valid in the context of a web-aware Spring ApplicationContext.

Difference between BeanFactory and ApplicationContext .-BeanFactory and ApplicationContext both are Java interfaces and ApplicationContext extends BeanFactory. BeanFactory provides basic IOC and DI features while ApplicationContext provides advanced features.

@Autowiring .-Autowiring feature of spring framework enables you to inject the object dependency implicitly. It internally uses setter or constructor injection. Autowiring can't be used to inject primitive and string values. It works with reference only.

@Qualifier .-Allows you to associate a given name with a specific bean type. is to fix that the bean with a given name will only qualify for dependency injection in Autowiring.

@Bean .-Is used to explicitly declare a single bean, rather than letting Spring do it automatically as above

@Component (and @Service and @Repository) are used to auto-detect and auto-configure beans using classpath scanning.  

@Component annotation is a stereotype and is used at class level that makes the class a component. These classes are eligible for auto-detection through classpath scanning.

@ComponentScan annotation is used to auto detect the component and in spring application context XML.

@Service annotation .-Is a stereotype and is used at class level that makes the class a service for service layer.

@Repository annotation .-Is a stereotype and is used at class level that makes the class a repository for persistence layer.

@Controller annotation .-Is a stereotype and is used at class level that makes the class a controller for presentation layer (spring-mvc).

@PostConstruct and @PreDestroy .-The support for these annotations offers yet another alternative to those described in initialization callbacks and destruction callbacks.

public class CachingMovieLister {

  @PostConstruct  public void populateMovieCache() {      // populates the movie cache upon initialization...  }

  @PreDestroy  public void clearMovieCache() {      // clears the movie cache upon destruction...  } }

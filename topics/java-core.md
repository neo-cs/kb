---
layout: topic
title: Java
permalink: topics/java-core.html
---

__JDK__ - Java Development Kit, this kits contains the libraries necessaries to
 develop an application in Java.

__JRE__ - Java Runtime Environment, this kit contains the libraries necessaries
 to run any application in Java.

__JVM__ - Java Virtual Machine, this machine is the environment that is able to
 execute the [bytecode](https://en.wikipedia.org/wiki/Java_bytecode) Java;
 that bytecode is created when any Java file is complied and creates.

## Java Language Keywords

List of keywords in the Java programming language.

| `abstract`  |  `continue` |  `for`        |  `new`        |  `switch`       |
| `assert***` |  `default`  |  `goto*`      |  `package`    |  `synchronized` |
| `boolean`   |  `do`       |  `if`         |  `private`    |  `this`         |
| `break`     |  `double`   |  `implements` |  `protected`  |  `throw`        |
| `byte`      |  `else`     |  `import`     |  `public`     |  `throws`       |
| `case`      |  `enum****` |  `instanceof` |  `return`     |  `transient`    |
| `catch`     |  `extends`  |  `int`        |  `short`      |  `try`          |
| `char`      |  `final`    |  `interface`  |  `static`     |  `void`         |
| `class`     |  `finally`  |  `long`       |  `strictfp**` |  `volatile`     |
| `const*`    |  `float`    |  `native`     |  `super`      |  `while`        |

\*     not used<br />
\*\*     added in 1.2<br />
\*\*\*     added in 1.4<br />
\*\*\*\*     added in 5.0<br />

__`static`__ - The keyword `static` is used for memory management mainly, it's a
 property that belongs to the class and not to object (instance).
> We can apply it with:
> * Variables
> * Methods
> * Blocks
> * Nested class

__`final`__ - The keyword `final` is used to restrict the behavior of variables,
 methods and classes.
* Variable its assigned value once and can't be changed.
* Methods that cannot be override
* Class cannot be extended.

__Garbage Collector__ - It's a feature of java to provide memory management with
 destroying automatically the unreferenced objects. For this behavior they said
  Java is __*simple*__.
> We have two ways to call the garbage collector:
> * Instancing the `Runtime` class and calling the `gc` method.
````java
Runtime r = Runtime.getRuntime();
r.gc();
````
> * Calling the `gc` method in System class.
````java
System.gc();
````

__Annotation__ - The annotation in Java is preceded by at symbol (@). Is a tag
 that represents the metadata and it provide additional information which can
 be used by Java compiler and JVM. It can be used in class, interface, methods
 or fields. Annotations became available in the language itself beginning with
 version 1.5 of the JDK.

Annotations applied to Java code:
 * __`@Override`__ - Checks that the method is an override.
 * __`@Deprecated`__ - Marks the method as obsolete.
 * __`@SuppressWarnings`__ - Instructs the compiler to suppress the compile time
 warnings specified in the annotation parameters.

__`super`__ - The keyword `super` is used to access methods of the parent class.

__`this`__ - The keyword `this` is a reference to the current object. The most
 common reason for using the this keyword is because a field is shadowed by a
 method or constructor parameter. Also this keyword is used to call another
 constructor in the same class.
````java
public class Point {
    public int x = 0;
    public int y = 0;

    public Point(int x, int y) {
        this.x = x; // x field is shadowed
        this.y = y; // y field is shadowed
    }
}
````

````java
public class Rectangle {
    private int x, y;
    private int width, height;

    public Rectangle() {
        this(0, 0, 1, 1); // Call another constructor
    }
    public Rectangle(int width, int height) {
        this(0, 0, width, height); // Call another constructor
    }
    public Rectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    ...
}
````

## Language Basics

__Method__ - Method is used to expose behavior of an object. Method must have
 return type, method is invoked explicitly, method is not provided by compiler
 in any case; method name can be or not the same as class name.

__Constructor__ - Is used to initialize the state of an object, constructor must
 not have return type, constructor is invoked implicitly, the constructor name
 must be same as the class name. The Java compiler provides a default
 constructor if you don't have any constructor.


### Primitive Data Types
Java defines eight primitive data types. The primitive types can be considered
in four categories:

| Category  | Primitive data type                |
|-----------|------------------------------------|
| Logical   | `boolean`                          |
| Textual   | `char`                             |
| Integral  | `byte`, `short`, `int`, and `long` |
| Floating  | `double` and `float`               |

Ranges of Numeric Primitives

| Type     | Bits | Bytes | Minimum Range | Maximum Range |
|----------|------|-------|---------------|---------------|
| `byte`   | 8    | 1     | –2^7          | 2^7 – 1       |
| `short`  | 16   | 2     | –2^15         | 2^15 – 1      |
| `int`    | 32   | 4     | –2^31         | 2^31 – 1      |
| `long`   | 64   | 8     | –2^63         | 2^63 – 1      |
| `float`  | 32   | 4     | n/a           | n/a           |
| `double` | 64   | 8     | n/a           | n/a           |

__Auto Boxing__ - Is used to convert primitive data types to their wrapper
 class objects. Wrapper class provide a wide range of function to be performed
 on the primitive types. The most common example is:

````java
  int a = 56;
  Integer i = a; // Auto Boxing
````

__Unboxing__ - Is opposite of Auto Boxing where we convert the wrapper class
 object back to its primitive type. This is done automatically by JVM so that
 we can use a the wrapper classes for certain operation and then convert them
 back to primitive types as primitives result `int` faster processing. For
 example:

 ````java
  Integer s = 45;
  int a = s; // Auto Unboxing
````

## Object-Oriented Programming Concepts

__Encapsulation__ - Is the process of hiding information details and protecting
 data and behavior of an object from misuse by other objects. In Java,
 encapsulation is done using access modifiers (public, protected, private) with
 classes, interfaces, setters, getters.

__Polymorphism__ - Polymorphism is the ability of an object to take on many
 forms. When a parent class reference is used to refer to a child class object.

__Inheritance__ - Is defined as the process where one class acquires the
 properties (methods and fields) of another.

![Types of Inheritance](https://www.tutorialspoint.com/java/images/types_of_inheritance.jpg)

__Abstraction__ - Is the process of modeling real things in the real word into
 programming language. In Java, the process of abstraction is done using
 interfaces, classes, abstract classes, fields, methods and variables.
 Everything is an abstraction.

__Differences between `class`, `abstract class` and `interface`__

| Class | Abstract | Interface |
|-------|----------|-----------|
| Only have non-abstract methods | Have abstract and non-abstract methods. | Have only abstract methods. Since Java 8, it can have default and static methods also.|
| Doesn't support multiple inheritance. | Doesn't support multiple inheritance. | Supports multiple inheritance. |
| Have final, non-final, static and non-static variables. | Have final, non-final, static and non-static variables. | Have only static and final variables. |
| Provide the implementation of interface. | Provide the implementation of interface. | Can't provide the implementation of abstract class. |
| The class keyword is used to declare `class`.| The abstract keyword is used to declare `abstract class`. | The interface keyword is used to declare `interface`. |

__When use Interface__ - When we are talking about establishing a contract about
 *what the object can do*.

__When use Abstract class__ - When we talk about abstract classes we are
 defining characteristics of an object type; specifying what an object is. The
  classes that extends from that classes.

__Access Modifiers__ - Access level modifiers determine whether other classes
 can use a particular field or invoke a particular method.

__Access Levels__

| Modifier    | Class | Package | Subclass | World |
|-------------|-------|---------|----------|-------|
| `public`    | Yes   | Yes     | Yes      | Yes   |
| `protected` | Yes   | Yes     | Yes      | __No__|
| (default)   | Yes   | Yes     | __No__   | __No__|
| `private`   | Yes   | __No__  | __No__   | __No__|

__Overloading__ - Method overloading is a feature that allows a class to have
 more than one method having the same name, if their argument lists are
 different. It is similar to constructor overloading in Java, that allows a
 class to have more than one constructor having different argument lists.

__Overriding__ - Is ability to define a behavior that's specific to the subclass
 type, which means a subclass can implement a parent class method based on its
 requirement.

## Exceptions

__Exceptions__ - The term exception is shorthand for the phrase "exceptional
 event". An exception is an event, which occurs during the execution of a
 program, that disrupts the normal flow of the program's instructions.

__What types of exceptions does exist in Java?__ - There are three types of
 exceptions: `Exception`, `Error` and `RuntimeException` which can be grouped in
 *checked* (`Exception`) and *unchecked* (`Error` and `RuntimeException`).

 > __NOTE__ Although `RuntimeException` extends `Exception` is in the unchecked
  category.

__`Exception`__ These are exceptional conditions that a well-written
  application should anticipate and recover from. This are subject to the
   Catch or Specify Requirement.

__`Error`__ These are exceptional conditions that are external to the
 application, and that the application usually cannot anticipate or recover
 from. This are not subject to the Catch or Specify Requirement.

__`RuntimeException`__ These are exceptional conditions that are internal
 to the application, and that the application usually cannot anticipate or
 recover from. These usually indicate programming bugs, such as logic errors
 or improper use of an API. This are not subject to the Catch or Specify
 Requirement.

__Exception Hierarchy__

![Exception Hierarchy](https://www.tutorialspoint.com/java/images/exceptions1.jpg)

__How handle an exception in Java?__ Using `try`, `catch` or `finally` keywords.

__`try`__ - Java try block is used to enclose the code that might throw an
 exception. It must be used within the method. Java try block must be followed
 by either catch or finally block.

__`catch`__ - A catch block is where you handle the exceptions, this block must
 follow the try block. A single try block can have several catch blocks
 associated with it. You can catch different exceptions in different catch
 blocks.

__`finally`__ - Contains all the crucial statements that must be executed
 whether exception occurs or not.

__Should we handle runtime exceptions?__ - We can handle a runtime exception
 using a try-catch block, but it is not mandatory.

__Difference between `throw`, `throws` and `Throwable`__
 *  `throw` is used within method to explicitly throw an exception, checked
 exception cannot be propagated using throw only, throw is followed by an
 instance, you cannot throw multiple exceptions. We can throw either checked or
 unchecked exception in Java by `throw` keyword.
 *  `throws` is used with the method signature to delegate an exception, is
 used to declare an exception handling requirement, checked exception can be
 propagated with `throws`, `throws` is followed by class, you can declare
 multiple exceptions handling requirements.
 *  `Throwable` - The `Throwable` class is the super class of all errors and
 exceptions.

## Strings

The strings are a sequence of characters. In the Java programming language,
 strings are objects. The Java platform provides the
 [`String`](https://docs.oracle.com/javase/8/docs/api/java/lang/String.html)
 class to create and manipulate strings. The `String` class is *immutable*, so
 that once it is created a String object cannot be changed. Since strings are
 immutable, what these methods really do is create and return a new string that
 contains the result of the operation.

__String Constant Pool__ - `String` literals objects are stored in this special
 memory area, to make Java more memory efficient.

__What are the differences between `StringBuffer` and `StringBuilder`?__ -
 `StringBuffer` is thread-safe (by virtue of having its methods synchronized)
 while `StringBuilder` is not thread-safe.

__What are the differences between `==`, `equals` and `hashCode`?__
 *  The `==` operator. Compares references of memory not values.
 *  The `hashCode` method. Returns a hash code value (an integer number) and
    it's related to how the equals method works.
 *  The `equals` method. Compares the original content of the string to the
    specified object.

## Java Collections Framework

![Java Collections Framework](https://qph.fs.quoracdn.net/main-qimg-0a1299cfbe49e6fec3f99af4e0d2b788)

__Collection Framework__ - A *collections framework* is a unified architecture
 for representing and manipulating collections.

`Collection` interface - Is one of the root interfaces of the Java collection
 classes. Though you do not instantiate a Collection directly.

### [`List`](https://docs.oracle.com/javase/8/docs/api/java/util/List.html) interface

__`List` interface__ - Is the sub interface of `Collection`. It contains methods
 to insert and delete elements in index basis.

You can choose between the following List implementations in the Java
 Collections API:
 * `java.util.ArrayList` - Resizable-array implementation of the `List`
   interface. Implements all optional list operations, and permits all elements,
   including null. In addition to implementing the `List` interface, this class
   provides methods to manipulate the size of the array that is used internally
   to store the list.
 * `java.util.LinkedList` - Doubly-linked list implementation of the List
   and Deque interfaces. Implements all optional list operations, and permits
   all elements (including `null`).
 * `java.util.Vector` - The `Vector` class implements a growable array of
   objects. Like an array, it contains components that can be accessed using an
   integer index.
 * `java.util.Stack` - The `Stack` class represents a last-in-first-out (LIFO)
   stack of objects. It extends class `Vector` with five operations that allow a
   vector to be treated as a stack.

### [`Set`](https://docs.oracle.com/javase/8/docs/api/java/util/Set.html) interface

__`Set` interface__ - A collection that contains no duplicate elements. The
 `Set` interface places additional stipulations, beyond those inherited from
  the Collection interface, on the contracts of all constructors and on the
  contracts of the `add`, `equals` and `hashCode` methods.

 * `java.util.EnumSet` - Java `EnumSet` class is the specialized `Set`
   implementation for use with enum types. It inherits `AbstractSet` class and
   implements the `Set` interface.
 * `java.util.HashSet` - Java `HashSet` class is used to create a collection
   that uses a hash table for storage. It inherits the `AbstractSet` class and
   implements `Set` interface.
 * `java.util.LinkedHashSet` - Java `LinkedHashSet` class is a hash table and
   linked list implementation of the `Set` interface. It inherits `HashSet`
   class and implements `Set` interface.
   uses a tree for storage. It inherits `AbstractSet` class and implements
 * `java.util.TreeSet` - `TreeSet` class implements the `Set` interface that
   `NavigableSet` interface. The objects of `TreeSet` class are stored in
   ascending order.

### [`Map`](https://docs.oracle.com/javase/8/docs/api/java/util/Map.html) interface

__`Map` Interface__ - A Map is an object that maps keys to values. A map cannot
 contain duplicate keys: Each key can map to at most one value. The Map
 interface includes methods for basic operations (such as `put`, `get`, `remove`,
 `containsKey`, `containsValue`, `size`, and `empty`).

 * `java.util.HashMap` - Hash table based implementation of the `Map` interface.
   This implementation provides all of the optional map operations, and permits
   `null` values and the `null` key. This class makes no guarantees as to the
   order of the map.
 * `java.util.Hashtable` - This class implements a hash table, which maps keys
   to values. Any nonnull object can be used as a key or as a value.
 * `java.util.LinkedHashMap` - Hash table and linked list implementation of the
   `Map` interface, with predictable iteration order. This implementation
   differs from `HashMap` in that it maintains a doubly-linked list running
   through all of its entries. This linked list defines the iteration ordering
 * `java.util.TreeMap` - Implements `Map` interface similar to `HashMap` class.
   The main difference between them is that `HashMap` is an unordered collection
   while `TreeMap` is sorted in the ascending order of its keys. `TreeMap` is
   unsynchronized collection class which means it is not suitable for
   thread-safe operations until unless synchronized explicitly.

Advantage of [Generic](#generics) collections - If we use generic class, we
 don't need typecasting. It is type safe and checked at compile time.

__Difference between array and `ArrayList`__
 * Array is a fixed length data structure whereas.
 * `ArrayList` is a variable length `Collection` class.

 __Difference between `ArrayList` and `Vector`__
 * `ArrayList`. is not synchronized, is not a legacy class, increases its size
   by 50% of the array size.
 * `Vector`. is synchronized, is a legacy class, increases its size by doubling
   (100%) the array size.

__Difference between `ArrayList` and `LinkedList`__
 * `ArrayList`. uses a dynamic array, is not efficient for manipulation because
   a lot of shifting is required, is better to store and fetch data.
 * `LinkedList`. uses doubly linked list, is efficient for manipulation, is
   better to manipulate data.

__Difference between `Hashmap` and `HastTable`__
  * `Hashmap`. is not synchronized, can contain one null key and multiple `null`
    values.
  * `HastTable`. is synchronized, cannot contain any null key or null value.

__Difference between `Iterator` and `ListIterator`__
 * `Iterator` traverses the elements in forward direction only whereas.
 * `ListIterator` traverses the elements in forward and backward direction.

### Object Ordering

__`Comparable`__ - When your class implements Comparable, the `compareTo` method
 of the class is defining the "natural" ordering of that object. That method is
 contractually obligated (though not demanded) to be in line with other methods
 on that object, such as a 0 should always be returned for objects when the
 `equals()` comparisons return `true`.

__`Comparator`__ - A `Comparator` is its own definition of how to compare two
 objects, and can be used to compare objects in a way that might not align with
 the natural ordering.

 __Differences between `Comparator` and `Comparable`__
  * `Comparable` interface obligate to implement the method `compareTo(Object o)`
  * `Comparator` interface obligate to implement the method `compare(Object o1, Object o2)`.

## Generics

 Generics class. A class or even Interface that can refer to any type as parameter.
  * The type parameter section, delimited by angle brackets (<>), follows the class name
  * This pair of angle brackets, <>, is informally called the diamond.
  * The type parameters naming conventions are important to learn generics thoroughly. The commonly type parameters are as follows:  
    - E - Element (used extensively by the [Java Collections Framework](#java-collections-framework))
    - K – Key
    - N – Number
    - T – Type
    - V – Value

## I/O (input/output)

Serialization. Is a mechanism of writing the state of an object into a byte stream.

`Serializable` interface. Is a *marker* interface (has no data member and method).
 It is used to *mark* java classes so that objects of these classes may get
  certain capability. The `Cloneable` and `Remote` are also marker interfaces.

`transient` - The `transient` keyword marks a member variable not to be
 serialized when it is persisted to streams of bytes.

## Concurrency

__Synchronization__ - Is the capability to control the access of multiple
 threads to any shared resource.

__How Synchronization works?__ Synchronization is built around an internal
 entity known as the lock or monitor. When a thread invokes a synchronized
 method, it automatically acquires the lock for that object and releases it
 when the thread completes its task.

__What type of synchronization does exist?__
 *  Mutual exclusive
    * Synchronized method.
    * Synchronized block.
    * Static synchronization.
 *  Cooperation (Inter-thread communication in java)

__Thread__ - Thread is a block of code which can execute concurrently with
other threads in the JVM.

__`Runnable` Interface__ - The Runnable interface should be implemented by
any class whose instances are intended to be executed by a thread. The class
must define a method of no arguments called run.

<img src="https://www.tutorialspoint.com/java/images/Thread_Life_Cycle.jpg" alt="Thread life cycle" width="450">

__Life cycle of a Thread (Thread States)__ - According to sun, there is only
5 states in thread life cycle in java new, runnable, running, wait(Blocked
or Waiting) and terminated. There is no running state.

 * New - The thread is in new state if you create an instance of `Thread` class
   but before the invocation of `start()` method. Runnable. The thread is in
   runnable state after invocation of start() method, but the thread scheduler
   has not selected it to be the running thread.
 * Runnable − After a newly born thread is started, the thread becomes
   runnable. A thread in this state is considered to be executing its task.
 * Running - The thread is in running state if the thread scheduler has selected
   it.
 * Non-Runnable (Blocked / Waiting) .-This is the state when the thread is still
   alive, but is currently not eligible to run.
 * Terminated (Dead) .-A thread is in terminated or dead state when its run()
   method exits.

__Ways to implement a thread__ There are two ways to create a thread:
 * Extending `Thread` class.
 * Implementing `Runnable` interface.

__Where use `Runnable`__
 * Implementing Runnable, many threads can share the same runnable instance
   it becomes a big performance overhead.
 * You can save a space for your class to extend any other class in future
   or now. If we are not making any modification on `Thread` then use `Runnable`
   interface instead.

__Where use `Thread`__ - Just in case if we need add new functionality,
 modifying or improving behaviors.

__Difference between `wait()` and `sleep()` method?__
 * The `wait()` is defined in `Object` class, it releases the lock.
 * The `sleep()` is defined in `Thread` class, it doesn't releases the lock.

__`volatile`__ - Guarantees the visibility for other threads of writes to one
 variable.

__Multithreading__ - Is a process of executing multiple threads simultaneously.
 Its main advantage is:

 * Threads share the same address space.
 * Thread is lightweight.
 * Cost of communication between process is low.

## The Reflection API

__Reflection__ - It is a process of examining or modifying the run time behavior
 of a class.

__How to use Reflection__ - Imagine that you have any object with whatever name,
 you can use dot to access to `getClass` Method and then `getMethod` to create a
 reference to that element. Once you created this reference you can call the
 method invoke whenever you want. All theses steps within try and catch block.

__What is the most common way to use it?__ - One very common use case in Java is
 the usage with [JUnit](https://junit.org) `@Test` annotation. Because you
 should look through your classes for methods tagged and will then call them
 when running the unit test.

__Note__ The Reflection API is mainly used in:
 *  IDE (Integrated Development Environment) e.g. Eclipse, MyEclipse, NetBeans etc.
 *  Debugger
 *  Test Tools etc.

<table>
  <tr>
    <th colspan="2"><code>Object</code> class</th>
  </tr>
  <tr>
    <th>Definition</th>
    <td>Is the root of the class hierarchy. Every class has Object
     as a superclass.</td>
  </tr>
  <tr>
    <th colspan="2">Method Summary</th>
  </tr>
  <tr>
    <th><code>clone()</code></th>
    <td>Creates and returns a copy of this object.</td>
  </tr>
  <tr>
    <th><code>equals(Object obj)</code></th>
    <td>Indicates whether some other object is "equal to" this one.</td>
  </tr>
  <tr>
    <th><code>finalize()</code></th>
    <td>Called by the garbage collector on an object when garbage
      collection determines that there are no more references to the object.</td>
  </tr>
  <tr>
    <th><code>getClass()</code></th>
    <td>Returns the runtime class of this Object.</td>
  </tr>
  <tr>
    <th><code>hashCode()</code></th>
    <td>Returns a hash code value for the object.</td>
  </tr>
  <tr>
    <th><code>notify()</code></th>
    <td>Wakes up a single thread that is waiting on this object's monitor.</td>
  </tr>
  <tr>
    <th><code>notifyAll()</code></th>
    <td>Wakes up all threads that are waiting on this object's monitor.</td>
  </tr>
  <tr>
    <th><code>toString()</code></th>
    <td>Returns a string representation of the object.</td>
  </tr>
  <tr>
    <th><code>wait()</code></th>
    <td>Causes the current thread to wait until another thread invokes the
        <code>notify()</code> method or the <code>notifyAll()</code> method for
        this object.</td>
  </tr>
  <tr>
    <th><code>wait(long timeout)</code></th>
    <td>Causes the current thread to wait until either another
      thread invokes the <code>notify()</code> method or the <code>notifyAll()</code>
      method for this object, or a specified amount of time has elapsed.</td>
  </tr>
  <tr>
    <th><code>wait(long timeout, int nanos)</code></th>
    <td>Causes the current thread to wait until
      another thread invokes the <code>notify()</code> method or the
      <code>notifyAll()</code> method for this object, or some other thread
      interrupts the current thread, or a certain amount of real time has
      elapsed.</td>
  </tr>
</table>

## Java Database Connectivity (JDBC)

__JDBC__ - Java JDBC is a java API to connect and execute query with the
 database. JDBC API uses jdbc drivers to connect with the database.

__JDBC Driver__ - For communicating with the database.

There are 4 types of JDBC drivers:
 *  __Type 1__ JDBC-ODBC bridge driver.
 *  __Type 2__ Native-API driver (partially Java driver).
 *  __Type 3__ Network Protocol driver (fully Java driver).
 *  __Type 4__ Thin driver (fully Java driver).

__DriverManager__ - For managing a list of database drivers.

__Connection__ - For interfacing with all the methods for connecting a database.

__Statement__ - For encapsulating an SQL statement for passing to the database
 which had been parsed, compiled, planned and executed.

__PreparedStatement__ - Interface is a subinterface of Statement. It is used to
 execute parameterized query.

__ResultSet__ - For representing a set of rows retrieved for the query execution.

__Why use return type `executeQuery`__ - (When we want to execute SQL statements
  which retrieve some data from the database, for example a select statement).
  This method returns a ResultSet object with the results of the query.

__Why use return type `executeUpdate`__ - (When we want to update or modify the
  database is some way using create, alter, insert, update or delete statements).
  Returns an int value which represents the number of rows affected by the query.

## Java Web Applications

__JSP__ - stands for JavaServer Pages. JSP is java server side technology to
 create dynamic web pages.

__Servlet__ - Is a Java program that extends the capabilities of a server.
 Although servlets can respond to any types of requests, they most commonly
 implement applications hosted on Web servers.

 __JSP - Implicit Objects__ - in JSP we have 9 implicit objects.
 * __Request__ This is the HttpServletRequest object associated with the request.
 * __Response__ This is the HttpServletResponse object associated with the response to the client.
 * __Out__ This is the PrintWriter object used to send output to the client.
 * __Session__ This is the HttpSession object associated with the request.
 * __Application__ This is the ServletContext object associated with the application context.
 * __Config__ This is the ServletConfig object associated with the page.
 * __PageContext__ This encapsulates use of server-specific features like higher performance JspWriters.
 * __Page__ This is simply a synonym for this, and is used to call the methods defined by the translated Servlet class.
 * __Exception__ The Exception object allows the exception data to be accessed by designated JSP.

### Servlet Lifecycle

__JSP Initialization__ - When a container loads a JSP it invokes the `jspInit()`
 method before servicing any requests. If you need to perform JSP-specific
 initialization, override the `jspInit()` method.

__JSP Execution (Ready)__ - the JSP engine invokes the _jspService() method in
 the JSP. The _jspService()  takes an `HttpServletRequest` and an
 `HttpServletResponse`.

__JSP Cleanup__ - The `jspDestroy()` method is the JSP equivalent of the destroy
 method for servlets. Override `jspDestroy` when you need to perform any cleanup.

### MVC in JSP

__MVC__ - MVC stands for Model View and Controller. It is a design pattern that
 separates the business logic, presentation logic and data.

 * __Controller__ acts as an interface between View and Model. Controller
   intercepts all the incoming requests.
 * __Model__ represents the state of the application i.e. data. It can also
   have business logic.
 * __View__ represents the presentation i.e. UI (User Interface).

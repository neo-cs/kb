---
layout: default
title: Spring topics
permalink: topics/sql.html
---

# SQL

What is SQL .- Structured Query Language(SQL) is a language designed specifically for communicating with databases.

Different type of SQL's statements:
 *  DDL – Data Definition Language .-DDL is used to define the structure that holds the data. For example, Create, Alter, Drop and Truncate table.
 *  DML – Data Manipulation Language .-DML is used for manipulation of the data itself. Typical operations are Insert, Delete, Update and retrieving the data from the table.
 *  DCL – Data Control Language .-DCL is used to control the visibility of data like granting database access and set privileges to create tables, etc. Example - Grant, Revoke access permission to the user to access data in the database. Table in a database .-A table is a collection of records of a specific type.

Define Normalization .-It's an Organized data to avoid inconsistent dependency and redundancy within a database.

Primary key .-It is a column whose values uniquely identify every row in a table.

Foreign Key .-It's a column value whose references another table's primary key value.

Unique Key .-It's a column like primary key only allows uniques values with the difference being the existence of null.

Why do we use Foreign keys .- In Order to link two tables, maintain data integrity and faster queries. Enlist the various relationships of database .-The various relationships of database are:
 *  One-to-one: Single table having drawn relationship with another table having similar kind of columns.
 *  One-to-many: Two tables having primary and foreign key relation.
 *  Many-to-many: Junction table having many tables related to many tables.

Different type of joins
 *  (Inner) Join .-return rows when there is at least one match in both table. it is used to combine data from two or more tables, based on a common field between them.
 *  Right (Outer) Join .-Return all rows from the right table, even if there are no matches in the left table.
 *  Left (Outer) Join .-Return all rows from the left table, even if there are no matches in the right table.
 *  Full (Outer) Join .-Return rows when there is a match in one of the tables.
 *  Cross Join. return all records where each row from the first table is combined with each row from the second table.

View .-The views are virtual tables, views simply contain PRE-COMPILE queries that dynamically retrieve data when used.

Stored procedure .-It is just a function which contains a collection of SQL Queries.

Trigger .-Those are sets of commands that get executed when an event(Before Insert, After Insert, On Update, On delete of a row) occurs on a table, views.

Difference between having and where clause .-one of which is meant to be used with aggregates and one of which is not and They have different semantics.
 *  Where. It's a clause selects before grouping, this clause cannot contain aggregate functions.
 *  Having. it is used to specify a condition for a group or an aggregate function used in select statement and it's a clause that selects rows after grouping. Difference between group by and distinct .-One of which is meant to be used with aggregates and one of which is not and They have different semantics, even if they happen to have equivalent results on your particular data.


 *  DISTINCT. It refers to distinct records as a whole, not distinct fields in the record. Note: Don't be mislead into thinking SELECT DISTINCT(A), B does something different. This isequivalent to SELECT DISTINCT A, B.
 *  GROUP BY. It refers to distinct fields in the record and lets you use aggregate functions, Note: functioning the same as a distinct when no aggregate function is present
 *  ORDER BY .-clause is used to sort the data in ascending or descending order, based on one or more columns.
